from django.shortcuts import render
from django.core.cache import cache

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import *
from .serializers import *
from .tasks import *


class PointViewSet(viewsets.ModelViewSet):
    queryset = Point.objects.all()
    serializer_class = PointSerializer

    @action(detail=True, methods=['GET'])
    def min_score(self, request, pk=None):
        to_point = request.query_params.get('to')
        res = min_score_path(int(pk), int(to_point))

        if 'error' in res:
            return Response(res)

        res['value'] += Point.objects.get(pk=to_point).score
        return Response(res)

    @action(detail=True, methods=['GET'])
    def min_length(self, request, pk=None):
        to_point = request.query_params.get('to')
        res = min_length_path(int(pk), int(to_point))

        if 'error' in res:
            return Response(res)

        return Response(res)

    
class LineViewSet(viewsets.ModelViewSet):
    queryset = Line.objects.all()
    serializer_class = LineSerializer

