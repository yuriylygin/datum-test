from django.db import models
from django.contrib.gis.db import models as gis_models


class Point(models.Model):
    id = models.IntegerField(primary_key=True)
    score = models.IntegerField()
    geom = gis_models.PointField(null=True)


class Line(models.Model):
    from_point = models.ForeignKey(Point, related_name='from_point', on_delete=models.CASCADE)
    to_point = models.ForeignKey(Point, related_name='to_point', on_delete=models.CASCADE)
    



