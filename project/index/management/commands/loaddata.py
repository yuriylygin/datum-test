from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis import geos

from project.index.models import *
from project.index.tasks import update_graphs

import requests
import json


class Command(BaseCommand):

    help = 'Load data from https://datum-test-task.firebaseio.com/api/lines-points.json'

    # def add_arguments(self, parser):

    #     parser.add_argument(
    #         '--clear',
    #         action='store_true',
    #         help='Clear database'
    #     )

    def handle(self, *args, **options):

        Point.objects.all().delete()
        Line.objects.all().delete()

        data = requests.get('https://datum-test-task.firebaseio.com/api/lines-points.json').json()
        lines = data.get('lines')
        points = data.get('points')

        for point in data.get('points'):
            Point.objects.create(
                id=point.get('obj_id'),
                score=point.get('score'),
                geom=geos.Point(point['lat'], point['lon'])
            )

        for line in data.get('lines'):
            Line.objects.create(
                from_point = Point.objects.get(id=line.get('from_obj')),
                to_point = Point.objects.get(id=line.get('to_obj'))
            )

        update_graphs()


        

