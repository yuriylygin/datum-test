from rest_framework import serializers
from .models import *


class PointSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Point
        fields = ['score', 'geom']


class LineSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Line
        fields = ['from_point', 'to_point']