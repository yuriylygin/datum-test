from django.contrib.gis import geos
from django.core.cache import cache

import igraph
import itertools

from .models import *



def update_graphs():
    cache.delete('length_graph')
    cache.delete('score_graph')

    points = Point.objects.all().values()
    lines = Line.objects.all().values()
    

    length_graph = igraph.Graph()
    score_graph = igraph.Graph(directed=True)

    length_graph.add_vertices(len(points))
    score_graph.add_vertices(len(points))

    length_graph.vs['score'] = list(map(lambda point: point['score'], points))
    length_graph.vs['id'] = list(map(lambda point: point['id'], points))

    score_graph.vs['score'] = length_graph.vs['score']
    score_graph.vs['id'] = length_graph.vs['id']

    # Graph enumerates its verteces from 'zero', but database enumeration begins from 'one'
    # So, we have to decrement points' id by one
    for line in lines:
        line['from_point_id'] -= 1
        line['to_point_id'] -= 1


    # let's build a graph for calculating the min lenght path
    length_graph_edges = list(map(lambda line: 
        {
            'adge': (line['from_point_id'], line['to_point_id']),
            'weight': geos.GEOSGeometry.distance(
                points[line['from_point_id']].get('geom'), 
                points[line['to_point_id']].get('geom')
            )
        }, 
        lines
    ))

    length_graph.add_edges([edge.get('adge') for edge in length_graph_edges]) 
    length_graph.es['weight'] = [edge.get('weight') for edge in length_graph_edges]


    # let's build a graph for calculating the min score path
    score_graph_edges = list(map(lambda line: 
        {
            'adge_forward': (line['from_point_id'], line['to_point_id']),
            'adge_backward': (line['to_point_id'], line['from_point_id']),
            'weight_forward': points[line['from_point_id']].get('score'),
            'weight_backward': points[line['to_point_id']].get('score'),
        }, 
        lines
    ))

    score_graph.add_edges(itertools.chain(
        [edge.get('adge_forward') for edge in score_graph_edges],
        [edge.get('adge_backward') for edge in score_graph_edges]
    )) 

    score_graph.es['weight'] = list(itertools.chain(
        [edge.get('weight_forward') for edge in score_graph_edges],
        [edge.get('weight_backward') for edge in score_graph_edges]        
    ))

    # for edge in score_graph_edges:
    #     print(edge)

    # for es in zip(score_graph.get_edgelist(), score_graph.es):
    #     print(es)

    # for vs in score_graph.vs:
    #     print(vs)

    # length_graph_layout = length_graph.layout("kk")
    # length_graph.vs['label'] = list(zip(length_graph.vs['id'], length_graph.vs['score']))
    # length_graph.es['label'] = length_graph.es['weight']
    # igraph.plot(length_graph, "length_graph.pdf", layout=length_graph_layout)

    # score_graph_layout = score_graph.layout("kk")
    # score_graph.vs['label'] = list(zip(score_graph.vs['id'], score_graph.vs['score']))
    # score_graph.es['label'] = score_graph.es['weight']
    # igraph.plot(score_graph, "score_graph.pdf", layout=score_graph_layout)

    # Store the graphs in cache without expiring date
    cache.set('length_graph', length_graph, timeout=None)
    cache.set('score_graph', score_graph, timeout=None)


def min_length_path(from_point, to_point):
    length_graph = cache.get('length_graph')

    if length_graph:
        print(length_graph)
        components = length_graph.components()
        for component in components:
            if from_point - 1 in component and to_point - 1 in component:
                path = length_graph.get_shortest_paths(
                    from_point - 1,
                    to=to_point - 1,
                    weights='weight',
                    mode=igraph.OUT,
                    output='vpath'
                )[0]
                value = length_graph.shortest_paths(
                    source=from_point - 1, 
                    target=to_point - 1, 
                    weights='weight', 
                    mode=igraph.OUT
                )[0][0]        
                return {'vertices': list(map(lambda point_id: point_id + 1, path)), 'value': value}

            return {'error': f'path from point {from_point} to {to_point} does not exist'}

    return {'error': 'length_graph is not loaded in cache'}


def min_score_path(from_point, to_point):
    score_graph = cache.get('score_graph')

    if score_graph:
        components = score_graph.components()
        for component in components:
            if from_point - 1 in component and to_point - 1 in component:
                path = score_graph.get_shortest_paths(
                    v = from_point - 1,
                    to=to_point - 1,
                    weights='weight',
                    mode=igraph.OUT,
                    output='vpath'
                )[0]
                value = score_graph.shortest_paths(
                    source=from_point - 1, 
                    target=to_point - 1, 
                    weights='weight', 
                    mode=igraph.OUT
                )[0][0]
                return {'vertices': list(map(lambda point_id: point_id + 1, path)), 'value': value}

            return {'error': f'path from point {from_point} to {to_point} does not exist'}

    return {'error': 'score_graph is not loaded in cache'}